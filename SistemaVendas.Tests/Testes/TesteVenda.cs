﻿using System;
using System.Collections.Generic;
using System.Text;
using Gallio.Framework;
using NUnit.Framework;
using NHibernate;
using SistemaVendas.Negocio;
using SistemaVendas.Model;
using SistemaVendas;

namespace TestProject1.Testes
{
    [TestFixture]
    public class TesteVenda : TesteBase
    {
        private ISession _session;
        
        [SetUp]
        public void Initialize()
        {
            _session = Contexto.SessionFactory.OpenSession();
        }

        [Test]
        public void TotalVendedorUm()
        {
            var ano = 2018;
            var idVendedor = 1;
            GeradorRelatorio gr = new GeradorRelatorio();
            Assert.AreEqual(100, gr.TotalVendasAnualPorVendedor(ano, idVendedor, _session));
        }
    }
}
