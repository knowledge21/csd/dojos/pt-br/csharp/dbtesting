﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace SistemaVendas
{
    public class GeradorRelatorio
    {
        public double TotalVendasAnualPorVendedor(int ano, int idVendedor, ISession _session)
        {

            string query = "select sum(v.valor) from venda v where v.IdVendedor = :idVendedor";

            double valorVendas = _session.CreateSQLQuery(query)
                .SetParameter("idVendedor", idVendedor)

                .UniqueResult<double>();


            return valorVendas;
        }
    }
}